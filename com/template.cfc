component {

	variables.settings.extensions = [];

	function init () {
		return this;
	}

	private function isCallable (fn) {
		return isCustomFunction(fn) || isClosure(fn);
	}

	/* extensions are evaluated in the order they are added */
	function addExtension (required any ext) {
		if (!isCallable(ext) &&
			!(isStruct(ext) && !isNull(ext.exec) && isCallable(ext["exec"]))) {
			throw(message="extension must be a function with signature `(string template, struct data) => string template` or an object or struct containing a function with that signature named `exec`");
		}

		arrayAppend(variables.settings.extensions, ext);
	}

	function exec (string template = "", struct data) {
		//if data is not provided, return a function that closes over the template
		if (isNull(data)) {
			return function (required struct data) {
				return exec (template, data);
			};
		}
		//iterate over keys of data
		for (var key in data) {
			template = replaceNoCase(template, "{{" & key & "}}", data[key], "all");			
		}

		//iterate over extensions
		for (var ext in variables.settings.extensions) {
			if (isCustomFunction(ext) || isClosure(ext)) {
				template = ext(template, data);
			} else {
				template = ext.exec(template, data);
			}
			if (!isSimpleValue(template)) {
				throw(message="extension did not return the resulting template properly.")
			}
		}

		return template;
	}

	/* convenience function for parsing parameters */
	function getFunctionParams (string functionName, string input = "", boolean trimSimpleValues = true, array defaults = []) {
		input = replaceNoCase(input, '{{' & functionName & '(', '');
		input = reverse(replaceNoCase(reverse(input), '}})', ''));
		var params = listToArray(input, ',');
		var paramsLength = arrayLen(params);
		for (var i = 1; i <= arrayLen(defaults); i++) {
			if (paramsLength < i) {
				params[i] = defaults[i];
			} else if (isSimpleValue(params[i])) {
				params[i] = trim(params[i]);
			}
		}
		return params;
	}

	/* convenience function for finding function calls */
	function getFunctionMatches (string functionName, string template = "") {
		return rematchNoCase('\{\{' & functionName & '\([\s\S]*?\)\}\}', template);
	}

}