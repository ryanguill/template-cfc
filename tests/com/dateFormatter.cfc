component extends="com.template" {

	function exec (string template = "", struct data = {}) {
		//look for anything that starts with {{dateFormat(variable)}}
		var matches = getFunctionMatches("dateformat", template);

		//provide the "now" key if it doesnt already exist in the data
		if (!structKeyExists(data, "now")) {
			data["now"] = now();
		}
		
		for (var match in matches) {
			params = getFunctionParams("dateformat", match, true, ["now", "yyyy-mm-dd"]);

			params[2] = replace(params[2], '"', '', 'all');
			params[2] = replace(params[2], "'", '', 'all');

			template = replaceNoCase(template, match, dateformat(data[params[1]], params[2]), 'all');
		}

		return template;
	}
}