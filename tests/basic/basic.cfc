component extends="testbox.system.BaseSpec" {


	private numeric function unixtime () {
		return createObject("java", "java.lang.System").currentTimeMillis();
	}


	function beforeAll () {
		variables.tmpl = new com.template();
	}

	function afterAll () {

	}

	function run () {

	
		describe("baseline environment", function () {

			it("should have created the template object", function() {
				expect(tmpl).toBeTypeOf("component");
			});

			it("should handle basic variable substitution", function() {
				var t = "{{firstname}} {{lastname}} {{badKey}} testing one two three.";
				var data = {
					"firstName": "Ryan",
					"LASTNAME": "Guill"
				};
				expect(tmpl.exec(t, data)).toBe("Ryan Guill {{badKey}} testing one two three.");
			});

			it("should take extensions", function() {
				
				var dateFormatter = new tests.com.dateformatter();
				tmpl.addExtension(dateformatter);
				
				var t = "{{firstname}} {{lastname}} {{badKey}} test {{dateFormat(createdDate, 'mm/dd/yy')}}";
				var data = {
					"firstName": "Ryan",
					"LASTNAME": "Guill",
					"createdDate": createDate(2017,12,31)
				};
				
				expect(tmpl.exec(t, data)).toBeWithCase("Ryan Guill {{badKey}} test 12/31/17");

				expect(tmpl.exec("{{dateFormat()}}", {})).toBeWithCase("#dateFormat(now(), "yyyy-mm-dd")#");
			});

			it("should be easy to provide an extension", function() {
				var ext = function (template = "", data = {}) {
						
					var functionName = "photo";

					if (!structKeyExists(data, "photoURL")) {
						data["photoURL"] = "";
					}

					var matches = tmpl.getFunctionMatches(functionName, template);
					for (var match in matches) {
						params = tmpl.getFunctionParams(functionName, match, true);
						var replacement = '<img src="' & data["photoURL"] & '"';
						if (arrayLen(params) >= 1) {
							replacement &= ' width="' & params[1] & '"';
						}
						if (arrayLen(params) >= 2) {
							replacement &= ' height="' & params[2] & '"';
						}
						replacement &= "/>";
						template = replaceNoCase(template, match, replacement, 'all');
					}
					return template;
				};
				

				tmpl.addExtension(ext);
				var data = {
					photoURL: "http://placehold.it/50x50"
				};
				expect(tmpl.exec("{{photo()}}", data)).toBeWithCase('<img src="http://placehold.it/50x50"/>');
				expect(tmpl.exec("{{photo(1)}}", data)).toBeWithCase('<img src="http://placehold.it/50x50" width="1"/>');
				expect(tmpl.exec("{{photo(1,2)}}", data)).toBeWithCase('<img src="http://placehold.it/50x50" width="1" height="2"/>');
			});

			it("should return a function if only provided with the template", function() {
				var fn = tmpl.exec('{{foo}}');

				expect(fn).toBeTypeOf('function');

				expect(fn({foo:"bar"})).toBe("bar");
			});

			it("should throw an error if you pass a bad extension", function () {
				expect(function() {
					tmpl.addExtension({});
				}).toThrow(message="extension must be a function with signature `(string template, struct data) => string template` or an object or struct containing a function with that signature named `exec`");
				
				expect(function() {
					tmpl.addExtension("foo");
				}).toThrow(message="extension must be a function with signature `(string template, struct data) => string template` or an object or struct containing a function with that signature named `exec`");
			});

			it("use an extension to provide a dsl for variable substitution", function() {
				var ext = function (template = "", data = {}) {
						
					//alias firstname as first.name
					data["first.name"] = structKeyExists(data, "firstname") ? data["firstname"] : "";
					data["last.name"] = structKeyExists(data, "lastname") ? data["lastname"] : "";

					//iterate over keys of data
					for (var key in data) {
						template = replaceNoCase(template, "{{" & key & "}}", data[key], "all");			
					}

					return template;
				};				

				tmpl.addExtension(ext);
				var data = {
					firstname: "Dave"
				};
				expect(tmpl.exec("{{firstname}}", data)).toBeWithCase("Dave");
				expect(tmpl.exec("{{first.name}}", data)).toBeWithCase("Dave");
				expect(tmpl.exec("{{lastname}}", data)).toBeWithCase("{{lastname}}");
				expect(tmpl.exec("{{last.name}}", data)).toBeWithCase("");
			});

		});

	}

}