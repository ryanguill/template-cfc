
# template.cfc

A small yet extensible library for interpolating a struct of values into a template.  Currently only supports handlebars notation (e.g. `{{foo}}`) but hope to make that configurable in the future.

You can also provide extensions that allow you to use "functions" that provide extra functionality beyond simple variable substitution.  See the tests for examples.

Note: the name is a work in progress - but not really that important.  If you have any good ideas for a better name please let me know.

## todo:

 [] Provide a way to change the variable qualifiers (e.g. `<% %>` instead of `{{ }}`)


## How to run the tests

1. Download testbox from: http://www.ortussolutions.com/products/testbox
2. Unzip the testbox/ folder into the root of the application (as a peer to tests)
3. The tests expect a redis instance to be running on localhost:6379, edit the top of /tests/basicTest.cfc if your instance is different
3. run /tests/index.cfm - will run the individual tests under basic remotely to be able to set the cookie headers

Or, if you use docker / docker-compose, you can use the included docker-compose file.

1. Clone the project.
2. `docker-compose up -d`
3. Hit the docker ip address on port 80.

You could swap out the `app` service with lucee or other coldfusion version if you would rather use that.

## Contributions

All contributions welcome! Issues, code, ideas, bug reports, whatever.

## License

This software is licensed under the Apache 2 license, quoted below.

Copyright 2016 Ryan Guill <ryanguill@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License"); you may not
use this file except in compliance with the License. You may obtain a copy of
the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
License for the specific language governing permissions and limitations under
the License.
